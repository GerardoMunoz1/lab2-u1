# Ejercicio 1 - Laboratorio 2
Gerardo Muñoz
### Instalación

Para ejecutar el programa, se debe tener todos los archivos presentes en el repositorio y escribir en una terminal en la ruta donde se encuentran todos los archivos guardados:

```
make
```

Con "make", asegura su correcta compilación, generando el archivo "app1". Luego escribir:

```
./pila
```

Cabe destacar que el programa, solamente está recogiendo los valores que le ingresa el usuario.


