#include <iostream>
#include "Pila.h"

using namespace std;

bool pila_llena(int tope, int max){


    bool band;

    if (tope == max){
        cout << "Pila llena.";
        band = true;
    }
    else{
        band = false;
        //ingresar_datos(pila, stoi(k));
    }
    return band;
}

void ingresar_numeros(Pila *pila, int k){

    int numero;
    int tope = 0;
    int max;
    max = k;
    bool aux = pila_llena(tope, max);

    for (int i=0; i<k; i++){

        if (aux == true){
            cout << "Desbordamiento, pila llena.";
        }
        else{
            cout << "INGRESE UN NUMERO (" << i+1 << "): ";
            cin >> numero;
            pila[i].set_dato(numero);
            tope++;

        }
    }

}

void imprimir_pila(Pila *pila, int k){
    for (int i=0; i<k; i++){

        if (pila[0].get_dato() == '\0'){
            cout << "ERROR";
        }
        else{
            cout << "|" << pila[i].get_dato() << "|" << endl;
        }
    }
}

void pila_vacia(Pila *pila, int k){
    bool flag;

    if (k == 0){    //pila vacía
        flag = 1;
    }
    else{           //pila no vacía
        flag = 0;
    }

}

void quitar_datos(Pila *pila, int k, int numero_eliminar){

    for (int i=0; i<k; i++){

        if (numero_eliminar == pila[i].get_dato()){
            pila[i].set_dato('\0');
            cout << "\nNUMERO ELMINADO CORRECTAMENTE.";
        }
    }
}

int main (){

    string k;
    bool flag;

    cout << "CAPACIDAD MÁXIMA DE LA PILA: ";
    getline(cin, k);

    Pila pila[stoi(k)];

    int option, numero_eliminar;

    do{
        cout << "\n     == PILA ==" << endl;
        cout << "\t [" << k << "]";
        cout << "\n[1] Agregar/push." << endl;
        cout << "[2] Remover/pop." << endl;
        cout << "[3] Ver pila." << endl;
        cout << "[4] Salir." << endl;
        cout << "-------------------" << endl;
        cout << "OPCIÓN: ";
        cin >> option;

        while (option != 1 && option != 2 && option != 3 && option != 4){
            cout << "OPCION INVALIDA. INTENTELO NUEVAMENTE: ";
            cin >> option;
        }
        if (option == 1){
            cout << "AGREGANDO" << endl;
            ingresar_numeros(pila, stoi(k));
        }
        else if (option == 2){
            cout << "ELIMINANDO" << endl;
            cout << "¿QUE NUMERO DESEA ELIMINAR?: ";
            cin >> numero_eliminar;
            quitar_datos(pila, stoi(k), numero_eliminar);
        }
        else if (option == 3){
            //pila_vacia(pila, stoi(k
            if (pila[0].get_dato() == '\0'){
                cout << "ERROR.";
            }
            else{
                cout << "IMPRIMIENDO PILA" << endl;
                imprimir_pila(pila , stoi(k));

            }
        }
        else if (option == 4){
            cout << "SALIENDO" << endl;
            exit(10);
        }

    }while(option != 4);

    return 0;
}
