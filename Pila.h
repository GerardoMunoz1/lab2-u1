#include <iostream>

using namespace std;

#ifndef PILA_H
#define PILA_H

class Pila{

    private:
        int numero;

    public:
        Pila();
        void set_dato(int numero);
        int get_dato();


};
#endif
